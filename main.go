package main

import (
	"crypto/sha256"
	"encoding/csv"
	"errors"
	"fmt"
	"os"

	"gitli.stratum0.org/heyalter/heyneuer-migrate/heyneuer"
)

func main() {
	if len(os.Args) <= 3 {
		fmt.Println("Usage: heyneuer-migrate [csv file] [team prefix] [api key]")
		return
	}
	f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	defer f.Close()
	data, err := csv.NewReader(f).ReadAll()
	if err != nil {
		panic(err)
	}

	api := heyneuer.API{
		BaseURL: "https://heyneuer.com",
		Prefix:  os.Args[2],
		Token:   os.Args[3],
	}

	var expectedNum uint64 = 1
	for i, line := range data {
		if i < 2 {
			continue // header
		}

		comp, errs := parseCSVEntry(line)
		if len(errs) > 0 {
			fmt.Println(line)
			fmt.Println(errs)
		}
		if comp == nil {
			continue
		}
		if comp.Number < expectedNum {
			fmt.Printf("Computer numbering is off: got %d, but already at %d. SKIPPING!!!\n", comp.Number, expectedNum)
			continue
		}

		comp.Donor = fmt.Sprintf("SHA256:%x", sha256.Sum256([]byte(comp.Donor)))
		// TODO
		if len(comp.Comment) > 220 {
			comp.Comment = comp.Comment[:217] + "CUT"
		}
		err := api.AddComputer(*comp)
		if err == nil {
			fmt.Println("Created computer", comp.Number)
		}
		if errors.Is(err, heyneuer.AlreadyExistingError) {
			err = api.UpdateComputer(*comp)
			if err == nil {
				fmt.Println("Updated computer", comp.Number)
			}
		}
		if err != nil {
			fmt.Println(line)
			panic(err)
		}
		expectedNum = comp.Number + 1
	}
}
