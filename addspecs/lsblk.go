package main

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type Lsblk struct {
	IsHDD      bool
	DiskSizeGB uint
}

type Device struct {
	Rota        bool
	Size        *string
	Mountpoint  *string
	Mountpoints *[]*string
	Children    *[]Device
}

func hasMountpoint(dev Device, mountpoint string) bool {
	if dev.Mountpoint != nil && *dev.Mountpoint == mountpoint {
		return true
	}

	if dev.Mountpoints != nil {
		for _, mount := range *dev.Mountpoints {
			if mount != nil && *mount == mountpoint {
				return true
			}
		}
	}

	if dev.Children == nil {
		return false
	}

	for _, ent := range *dev.Children {
		if hasMountpoint(ent, mountpoint) {
			return true
		}
	}
	return false
}

func parseGBSize(size string) uint {
	size = strings.TrimSuffix(size, "G")
	size = strings.ReplaceAll(size, ",", ".")
	gb, err := strconv.ParseFloat(size, 64)
	if err != nil {
		panic(err)
	}
	return uint(math.Round(gb * (float64(1024*1024*1024) / float64(1000*1000*1000))))
}

func ParseLsblk(filename string) (*Lsblk, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	var content struct {
		Blockdevices []Device
	}
	if json.NewDecoder(f).Decode(&content); err != nil {
		return nil, err
	}

	for _, ent := range content.Blockdevices {
		if !hasMountpoint(ent, "/") {
			continue
		}

		return &Lsblk{
			IsHDD:      ent.Rota,
			DiskSizeGB: parseGBSize(*ent.Size),
		}, nil
	}

	return nil, fmt.Errorf("No / mountpoint found")
}
