package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitli.stratum0.org/heyalter/heyneuer-migrate/heyneuer"
)

func main() {
	if len(os.Args) <= 3 {
		fmt.Println("Usage: addspecs [spec directory] [team prefix] [api key]")
		return
	}
	baseDir := os.Args[1]
	dirs, err := os.ReadDir(baseDir)
	if err != nil {
		panic(err)
	}

	api := heyneuer.API{
		BaseURL: "https://heyneuer.com",
		Prefix:  os.Args[2],
		Token:   os.Args[3],
	}

	newestSpecs := make(map[string]time.Time)
	for _, dir := range dirs {
		if !dir.IsDir() {
			continue
		}
		id, timeStr, ok := strings.Cut(dir.Name(), "_")
		if !ok {
			fmt.Println("Couldn't parse", dir.Name())
			continue
		}

		_, err := strconv.ParseUint(id, 10, 64)
		if err != nil {
			fmt.Println("Couldn't parse id", id)
			continue
		}
		parsedTimeInt, err := strconv.ParseInt(timeStr, 10, 64)
		if err != nil {
			fmt.Println("Couldn't parse time", timeStr)
		}
		parsedTime := time.Unix(parsedTimeInt, 0)

		if oldTime, ok := newestSpecs[id]; !ok || parsedTime.After(oldTime) {
			newestSpecs[id] = parsedTime
		}
	}

	for id, stamp := range newestSpecs {
		dir := filepath.Join(baseDir, fmt.Sprintf("%s_%d", id, stamp.Unix()))

		hw, err := ParseLshw(filepath.Join(dir, "lshw.json"))
		if err != nil {
			panic(err)
		}
		blk, err := ParseLsblk(filepath.Join(dir, "lsblk.json"))
		if err != nil {
			panic(err)
		}

		number, _ := strconv.ParseUint(id, 10, 64)
		// 2 == SSD, 1 == HDD
		var diskType int = 2
		if blk.IsHDD {
			diskType = 1
		}
		err = api.UpdateComputer(heyneuer.Computer{
			Number:             number,
			CPU:                hw.CPUModel,
			MemoryInGB:         hw.RamGB,
			HasWLAN:            &hw.HasWIFI,
			HardDriveType:      diskType,
			HardDriveSpaceInGB: blk.DiskSizeGB,
		})
		if err != nil {
			fmt.Printf("Error updating computer %s, got error: %s\n", id, err)
		} else {
			fmt.Println("Updated computer", id)
		}
	}
}
