package main

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
)

type Lshw struct {
	CPUModel string
	RamGB    uint
	HasWIFI  bool
}

func grepSection(part interface{}, key string) map[string]interface{} {
	for _, ent := range part.(map[string]interface{})["children"].([]interface{}) {
		mapping := ent.(map[string]interface{})
		if mapping["id"] == key {
			return mapping
		}
	}
	return nil
}

func checkPCIBus(start map[string]interface{}, hw *Lshw) {
	for _, ent := range start["children"].([]interface{}) {
		mapping := ent.(map[string]interface{})
		if caps, ok := mapping["capabilities"]; ok {
			if val, ok := caps.(map[string]interface{})["wireless"]; ok && val == "WLAN" {
				hw.HasWIFI = true
			}
		}
		if _, ok := mapping["children"]; ok {
			checkPCIBus(mapping, hw)
		}
	}
}

func ParseLshw(filename string) (*Lshw, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	var content interface{}
	if json.NewDecoder(f).Decode(&content); err != nil {
		return nil, err
	}

	if inner, ok := content.([]interface{}); ok {
		content = inner[0]
	}

	ret := &Lshw{}
	core := grepSection(content, "core")
	cpu := grepSection(core, "cpu")
	if cpu == nil {
		cpu = grepSection(core, "cpu:0")
	}
	ret.CPUModel = cpu["product"].(string)

	mem := grepSection(core, "memory")
	ret.RamGB = uint(math.Round(mem["size"].(float64) / float64(1024*1024*1024)))

	pci := grepSection(core, "pci")
	if pci == nil {
		i := 0
		for {
			pci = grepSection(core, fmt.Sprintf("pci:%d", i))
			if _, ok := pci["children"]; ok {
				checkPCIBus(pci, ret)
			}
			i++
			if grepSection(core, fmt.Sprintf("pci:%d", i)) == nil {
				break
			}
		}
	} else {
		checkPCIBus(pci, ret)
	}
	return ret, nil
}

/*

	tomap := map[*string]interface{} {
		&ret.ModelName: nil,
	}
	mapping := map[string]*string {
		"Modellname:": &ret.ModelName,
		"Model name:": &ret.ModelName,
	}
	for _, ent := range content.Lscpu {
		ref, ok := mapping[ent.Field]
		if !ok {
			continue
		}
		*ref = ent.Data
		delete(tomap, ref)
	}
	if len(tomap) != 0 {
		panic("Missing Keys!")
	}
*/
