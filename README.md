# HeyNeuer Migrationsskript

Dieses Skript migriert die existierende Computer-Tabelle im CSV-Format nach HeyNeuer.

## Installation/Ausführung

Nach der Installation einer aktuellen Go Version kann das Tool entweder mittels `go install gitli.stratum0.org/heyalter/heyneuer-migrate@latest` nach $GOBIN (Standardmäßig `~/go/bin`) installiert werden.
Dann kann es mittels `heyneuer-migratie [csv] [team prefix] [api key]` aufgerufen werden.
Alternativ kann das Repository geklont werden und im Source-Ordner `go run . [csv] [team prefix] [api key]` aufgerufen werden.

## Computerspecs

Für die gesammelten Computerspecs aus BS (siehe https://gitli.stratum0.org/heyalter/setupnetzwerk/-/blob/prod/nginx/www/setup) gibt es unter addspecs ein Skript, welches einen Spec-Ordner parst und die PC-Specs entsprechend im HeyNeuer aktualisiert.
