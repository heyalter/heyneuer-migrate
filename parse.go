package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitli.stratum0.org/heyalter/heyneuer-migrate/heyneuer"
)

func parseTyp(typ string) (heyneuer.Type, string, error) {
	switch typ {
	case "Laptop":
		return heyneuer.LAPTOP, "", nil
	case "Tower":
		return heyneuer.DESKTOP, "\nTower", nil
	case "All-in-One":
		return heyneuer.DESKTOP, "\nAll-in-One PC", nil
	case "SFF":
		return heyneuer.SMALL_FORM_FACTOR, "", nil
	case "Tablet":
		return heyneuer.TABLET, "", nil
	default:
		return heyneuer.UNKNOWN_TYPE, "", fmt.Errorf("Unknown type: %s", typ)
	}
}

func isEmptyLine(fields []string) bool {
	for i, val := range fields {
		if i != 0 { // 0 is the number and may be preassigned
			if strings.TrimSpace(val) != "" {
				return false // we have a nonempty field
			}
		}
	}
	return true
}

var TRASH_STRS []string = []string{
	"schrott",
	"entsorgt",
}

func isTrash(comment string) bool {
	comment = strings.ToLower(comment)

	for _, needle := range TRASH_STRS {
		if strings.Contains(comment, needle) {
			return true
		}
	}
	return false
}

func parseYes(line string) (bool, error) {
	line = strings.ToLower(strings.TrimSpace(line))
	if line == "1" || line == "ja" || line == "gefordert" {
		return true, nil
	} else if line == "0" || line == "" {
		return false, nil
	}

	return false, fmt.Errorf("Unknown yes entry: '%s'", line)
}

func unpack(arr []string, vars ...*string) []error {
	errors := []error{}
	for i, val := range arr {
		if vars[i] == nil {
			if val != "" {
				errors = append(errors, fmt.Errorf("Expected an empty cell, but got entry: '%s'", val))
			}
		} else {
			*vars[i] = val
		}
	}
	return errors
}

func parseCSVEntry(fields []string) (*heyneuer.Computer, []error) {
	if isEmptyLine(fields) {
		return nil, nil
	}
	var number, donor, typ, manufacturer, model, hasWebcam, professionalDeletion, protocol, requiredHW, thankMail, donorReceipt, canBeDelivered, delivered, dateAndSchool, comment, ATDSerial string
	errors := unpack(fields, &number, &donor, &typ, &manufacturer, &model, &hasWebcam, &professionalDeletion, &protocol, &requiredHW, &thankMail, &donorReceipt, &canBeDelivered, nil, &delivered, &dateAndSchool, &comment, nil, nil, nil, &ATDSerial)

	nummer, err := strconv.ParseUint(number, 10, 64)
	if err != nil {
		errors = append(errors, err)
	}
	parsedTyp, exCmt, err := parseTyp(typ)
	if err != nil {
		errors = append(errors, err)
	}
	comment += exCmt
	kamera, err := parseYes(hasWebcam)
	if err != nil {
		errors = append(errors, err)
	}
	profLoesch, err := parseYes(professionalDeletion)
	if err != nil {
		errors = append(errors, err)
	}
	spendenquittung, err := parseYes(donorReceipt)
	if err != nil {
		errors = append(errors, err)
	}
	abgabefertig, err := parseYes(canBeDelivered)
	if err != nil {
		errors = append(errors, err)
	}
	ausgeliefert, err := parseYes(delivered)
	if err != nil {
		errors = append(errors, err)
	}
	schrott := isTrash(comment)

	if protocol != "" {
		comment += "\nProtokoll?: " + protocol
	}
	if thankMail != "" {
		comment += "\nDanke E-Mail: " + thankMail
	}
	if dateAndSchool != "" {
		comment += "\nJJJJ-MM-TT Schule: " + dateAndSchool
	}
	if ATDSerial != "" {
		comment += "\nSeriennummer ATD Inventarisierung: " + ATDSerial
	}

	state := heyneuer.NEW
	if abgabefertig {
		state = heyneuer.REFURBISHED
	}
	if ausgeliefert {
		state = heyneuer.DELIVERED
	}
	if schrott {
		state = heyneuer.DESTROYED
	}

	return &heyneuer.Computer{
		Number:               nummer,
		Donor:                donor,
		Type:                 parsedTyp,
		Model:                manufacturer + " " + model,
		State:                state,
		Comment:              comment,
		HasWebcam:            &kamera,
		IsDeletionRequired:   &profLoesch,
		NeedsDonationReceipt: &spendenquittung,
		RequiredEquipment:    requiredHW,
	}, errors
}
