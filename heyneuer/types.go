package heyneuer

type ComputerState string

const (
	NEW          ComputerState = ComputerState("new")
	IN_PROGRESS                = ComputerState("in_progress")
	REFURBISHED                = ComputerState("refurbished")
	DELIVERED                  = ComputerState("delivered")
	DESTROYED                  = ComputerState("destroyed")
	COMMISSIONED               = ComputerState("picked")
)

type Type string

const (
	UNKNOWN_TYPE           = Type("0")
	DESKTOP           Type = Type("1")
	LAPTOP                 = Type("2")
	TABLET                 = Type("3")
	SMALL_FORM_FACTOR      = Type("4")
)

type Computer struct {
	Number               uint64        `json:"number,omitempty"`
	Donor                string        `json:"donor,omitempty"`
	Email                string        `json:"email,omitempty"`
	Type                 Type          `json:"type,omitempty"`
	Model                string        `json:"model,omitempty"`
	State                ComputerState `json:"state,omitempty"`
	Comment              string        `json:"comment,omitempty"`
	IsDeletionRequired   *bool         `json:"is_deletion_required,omitempty"`
	NeedsDonationReceipt *bool         `json:"needs_donation_receipt,omitempty"`
	HasWebcam            *bool         `json:"has_webcam,omitempty"`
	HasWLAN              *bool         `json:"has_wlan,omitempty"`
	//HasVGA bool `json:"has_vga_videoport"`
	//HasDVI bool `json:"has_dvi_videoport"`
	//HasHDMI bool `json:"has_hdmi_videoport"`
	//HasDisplayport bool `json:"has_displayport_videoport"`
	RequiredEquipment  string `json:"required_equipment,omitempty"`
	CPU                string `json:"cpu,omitempty"`
	MemoryInGB         uint   `json:"memory_in_gb,omitempty"`
	HardDriveType      int    `json:"hard_drive_type,omitempty"`
	HardDriveSpaceInGB uint   `json:"hard_drive_space_in_gb,omitempty"`
}
