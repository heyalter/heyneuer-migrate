package heyneuer

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type API struct {
	BaseURL string
	Token   string
	Prefix  string
}

var AlreadyExistingError error = errors.New("The computer does already exist")
var NotExistingError error = errors.New("The computer does not exist")

func (api API) AddComputer(computer Computer) error {
	encoded, err := json.Marshal(computer)
	if err != nil {
		return err
	}

	path, err := url.JoinPath(api.BaseURL, "api/computers")
	if err != nil {
		panic(err)
	}
	req, err := http.NewRequest("POST", path, bytes.NewReader(encoded))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+api.Token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusCreated:
		return nil
	case http.StatusConflict:
		return AlreadyExistingError
	default:
		respData, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("Error response: %d\n\n%s", resp.StatusCode, string(respData))
	}
}

func (api API) UpdateComputer(computer Computer) error {
	num := computer.Number
	computer.Number = 0
	encoded, err := json.Marshal(computer)
	computer.Number = num
	if err != nil {
		return err
	}

	path, err := url.JoinPath(api.BaseURL, fmt.Sprintf("api/computers/%s-%d", api.Prefix, computer.Number))
	if err != nil {
		panic(err)
	}
	req, err := http.NewRequest("PATCH", path, bytes.NewReader(encoded))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+api.Token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusOK:
		return nil
	case http.StatusNotFound:
		return NotExistingError
	default:
		respData, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("Error response: %d\n\n%s", resp.StatusCode, string(respData))
	}
}
